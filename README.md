# Processor

Some experimentation using Scenic to visualise Elixir Processes

![Screencast](https://imgur.com/2nQYcth)

## Prerequisites

Follow the instruction and install the [`scenic_driver_glfw`](https://github.com/boydm/scenic_new#install-prerequisites)

## Clone and run

```
git clone https://gitlab.com/martinstannard/processor.git
cd processor
mix deps.get
  
mix scenic.run
```
